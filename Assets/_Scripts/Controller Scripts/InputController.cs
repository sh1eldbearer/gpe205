﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour 
{
	/* Public variables */
	[Tooltip("Holds the TankData component of the tank being controlled by this InputController.")] 
	public TankData tankData;

	/* Private variables */
	// Controls whether the tank automatically moves forward (defaults to false)
	private bool autoMove = false;
	// Stores the prefix for this player's input axes
	private string axisPrefix;
	// A timestamp for the last time the tank fired a shot
	private float prevShotTime;
	 

	// Use this for initialization
	void Start () 
	{
		// Adds this tank object to the GameManger's player list
		GameManager.gm.players.Add (tankData);
		// Assigns a player number to this tank
		tankData.playerID = GameManager.gm.players.IndexOf (tankData) + 1;
		// Assigns the tank this controller to the player layer
		tankData.gameObject.layer = LayerMask.NameToLayer("Players");
		// Creates input axis prefix for this player
		axisPrefix = "P" + tankData.playerID + "_";
		// Sets the previous shot time to a value before the game stared, allowing the tank to immediately shoot
		prevShotTime = Time.time - tankData.shotDelay;
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Allows the player enable or disable AutoMove, which automatically moves the tank forward
		if (Input.GetButtonDown (axisPrefix + "AutoMove")) 
		{
			autoMove = !autoMove; // Inverts the current status of AutoMove
		}

		// If AutoMove is enabled, tank will automatically move forward
		if (autoMove)
		{
			// Automatically moves the tank forward at the designer-defined rate
			tankData.mover.Move (tankData.mover.tankTf.forward * tankData.moveFwdSpeed);
		}
		// If no AutoMove, looks for user input to move the tank forward
		else if (Input.GetAxis (axisPrefix + "Move") != 0f)
		{
			if (Input.GetAxis (axisPrefix + "Move") < 0f)
			{
				// Moves the tank backward at the designer-defined rate
				tankData.mover.Move (-tankData.mover.tankTf.forward * tankData.moveBackwardSpeed);
			}
			else if (Input.GetAxis (axisPrefix + "Move") > 0f)
			{
				// Moves the tank forward at the designer-defined rate
				tankData.mover.Move (tankData.mover.tankTf.forward * tankData.moveFwdSpeed);
			}
		}

		// Rotates the tank around its y-axis
		if (Input.GetAxis (axisPrefix + "Rotation") != 0.0f)
		{
			// Rotates the tank left or right at the designer-defined rate
			tankData.mover.Turn(Input.GetAxis(axisPrefix + "Rotation") * tankData.turnSpeed);
		}

		// Fires bullets from the tank
		if (Input.GetButton (axisPrefix + "Shooter"))
		{
			// Checks the current gametime against the time of the previous shot to determine if shooting is off cooldown
			if (Time.time >= prevShotTime + tankData.shotDelay)
			{
				// Shoots a bullet
				tankData.shooter.Shoot ();
				// Sets the timestamp for the previous shot to prevent bullet spamming
				prevShotTime = Time.time;
			}
		}
	}
}
