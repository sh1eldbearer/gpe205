﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankShooter : MonoBehaviour 
{
	/* Public variables */
	[Tooltip("The prefab bullet object to be fired by the tank")]
	public GameObject bulletPrefab;
	[Tooltip("The spawn location of the bullet")]
	public Transform bulletSpawnPoint;

	/* Private variables */
	// Holds this tanks' GameObject to pass to the newly-created bullet to designate ownership
	private TankData ownerTank; 
	// Holds the newly-created bullets' GameObject to assign tags and layers
	private GameObject newBullet; 
	// Holds the BulletOwner component of the newly-created bullet
	private BulletOwner bulletOwner; 

	// Use this for initialization
	void Start () 
	{
		// Gets and stores this tanks' GameObject
		ownerTank = this.gameObject.GetComponent<TankData> ();
	}

	/// <summary>
	/// Fires a bullet from this tank object.
	/// </summary>
	public void Shoot()
	{
		// Fires a bullet from the tank's attached spawn point
		newBullet = Instantiate (bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation,bulletSpawnPoint);
		// Moves the bullet forward along its z-axis at the speed the designer defines in the inspector
		newBullet.GetComponent<Rigidbody>().AddForce(newBullet.transform.forward * ownerTank.bulletSpeed);
		// Assigns the new bullet's BulletData component to a variable for quick reference
		bulletOwner = newBullet.GetComponent<BulletOwner> ();
		// Assigns this tank's TankData component to the bullet as its owner 
		bulletOwner.owner = ownerTank;
		// Breaks parent-child relationship between bullet and spawn point so the bullet does not rotate with the tank
		newBullet.transform.parent = null; 
		// Assigns the tank to the appropriate layer
		newBullet.layer = this.gameObject.layer;

	}
}
