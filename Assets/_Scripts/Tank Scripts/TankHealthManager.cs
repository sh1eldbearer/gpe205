﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankHealthManager : MonoBehaviour 
{
	/* Private variables */
	// Holds this tank's TankData component
	private TankData tankData;

	// Use this for initialization
	void Start () 
	{
		// Grabs and stores this tank's TankData component
		tankData = this.gameObject.GetComponent<TankData> ();
	}

	/// <summary>
	/// Lowers the hit points of the tank on which this TankHealthManager resides. 
	/// Will kill the tank if the tank's HP reaches or falls below zero, and add this tank's point value to the other tank's score total.
	/// </summary>
	/// <param name="dmgValue">Number of points of damage to inflict to the tank.</param>
	public void Damage(TankData bulletOwner)
	{
		// Damages the tank
		tankData.currentHP -= bulletOwner.bulletDamage;
		// If the tank's HP is at or below zero...
		if (tankData.currentHP <= 0)
		{
			// Adds the point value of this tank to the score of the tank that destroyed it
			bulletOwner.currentScore += tankData.pointValue;
			// ...kills the tank
			Die ();
		}
	}

	/// <summary>
	/// Raises the hit points of the tank on which this TankHealthManager resides.
	/// Tank's HP cannot exceed its maximum HP value.
	/// </summary>
	/// <param name="healValue">Number of points of healing to do to the tank.</param>
	public void Heal(int healValue)
	{
		// Heals the tank
		tankData.currentHP += healValue;
		// If the tank's HP is above the maximum HP value...
		if (tankData.currentHP > tankData.maxHP)
		{
			// ... sets the tank's current health to be equal to the maximum HP value
			tankData.currentHP = tankData.maxHP;
		}
	}
		
	// TODO: Consider a second "Die" function that despawns the tank and forces it to respawn elsewhere (later milestone)

	/// <summary>
	/// This kills the tank.
	/// </summary>
	public void Die()
	{
		// If this tank is an enemy tank...
		if (this.gameObject.layer == LayerMask.NameToLayer ("Enemies"))
		{
			// ... removes this tank from the list of enemies
			GameManager.gm.enemies.Remove(tankData);
		}
		else if (this.gameObject.layer == LayerMask.NameToLayer ("Players"))
		{
			// ... removes this tank from the list of players
			GameManager.gm.players.Remove (tankData);
		}
		// Tank is destroyed
		Destroy (this.gameObject);
	}
}
