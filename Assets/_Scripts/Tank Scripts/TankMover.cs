﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover: MonoBehaviour
{
	/* Public variables */
	// Holds this tank's transform component
	public Transform tankTf;

	/* Private variables */	
	// Holds this tank's CharacterController component
	private CharacterController cc; 

	// Use this for initialization
	void Start () 
	{
		// Gets and stores this tank's Transform component
		tankTf = this.gameObject.GetComponent<Transform> ();
		// Gets and stores this tank's CharacterController component
		cc = this.gameObject.GetComponent<CharacterController> ();
	}

	/// <summary>
	/// Moves the tank forward or backward, depending on the scale of the provided vector
	/// </summary>
	/// <param name="moveVector">Movement vector to determine how fast the tank moves, and in which direction.</param>
	public void Move(Vector3 moveVector)
	{
		// Moves the tank, and automatically applies gravity
		cc.SimpleMove (moveVector);
	}

	/// <summary>
	/// Rotates the tank around its y-axis at the provided speed parameter
	/// </summary>
	/// <param name="turnInfo">Speed at which the tank turns, in degrees/second.</param>
	public void Turn(float turnInfo)
	{
		// Rotates the tank around its y-axis, allowing the tank to turn
		tankTf.Rotate (tankTf.up * turnInfo * Time.deltaTime);
	}
}
