﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Requires a TankMover component to be on any object that uses a TankData component
[RequireComponent(typeof(TankMover))]
// Requires a TankShooter component to be on any object that uses a TankData component
[RequireComponent(typeof(TankShooter))]
// Requires a TankHealthManager component to be on any object that uses a TankData component
[RequireComponent(typeof(TankHealthManager))]

public class TankData : MonoBehaviour 
{
	/* Public variables */
	// Holds this tank's TankMover component
	[HideInInspector] public TankMover mover;
	// Holds this tank's TankShooter component
	[HideInInspector] public TankShooter shooter;
	// Stores player's ID number to recieve input from correct InputController axes
	[HideInInspector] public int playerID = 0;
	// Stores this player's score
	public int currentScore = 0;
	[Header("--------------- Tank Settings ---------------")]
	// Holds the current HP value of this tank (not hidden in inspector for observation and demonstration purposes)
	/* [HideInInspector] */ [Tooltip("The current number of hit points the tank has.")] public int currentHP;
	[Tooltip("The maximum number of hit points this tank starts with."),
		Range(1,5)] public int maxHP = 3;
	[Tooltip("The forward movement speed of the tank in meters/second."),
		Range(1.0f,100.0f)] public float moveFwdSpeed = 5f;
	[Tooltip("The backward movement speed of the tank in meters/second."),
		Range(1.0f,90.0f)] public float moveBackwardSpeed = 2.5f;
	[Tooltip("The turning speed of the tank in degrees/second."), 
		Range(1.0f,90.0f)] public float turnSpeed = 45f;
	[Tooltip("The cooldown time between shots, in seconds."), 
		Range(0f,2f)] public float shotDelay = 0.5f;
	[Tooltip("The amount of points this tank is worth to the player that kills it")]
		public int pointValue = 1000;
	[Space][Header("--------------- Bullet Settings ---------------")]
	[Tooltip("The move speed of the bullet, in meters/second."),
		Range(100f,1000f)] public float bulletSpeed = 20f;
	[Tooltip("The amount of damage a bullet fired from this tank does to other tanks."),
		Range(0,3)] public int bulletDamage = 1;
	[Tooltip("The maximum length of time, in seconds, that the bullet is allowed to exist for despawning."),
		Range(1f,10f)] public float bulletLifespan = 6f;

	// Use this for initialization
	void Start () 
	{
		// Gets and stores the tank's TankMover component
		mover = this.gameObject.GetComponent<TankMover> ();
		// Gets and stores the tank's TankShooter component
		shooter = this.gameObject.GetComponent<TankShooter> ();
		// Sets the tank's HP to its maximum value
		currentHP = maxHP;
	}
}
