﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Requires a BulletCollider component to be on any object that uses a BulletOwner component
[RequireComponent(typeof(BulletCollider))]
// Requires a BulletLifeTimer component to be on any object that uses a BulletOwner component
[RequireComponent(typeof(BulletLifeTimer))]

public class BulletOwner : MonoBehaviour 
{
	// 	Holds the TankData of the bullet's owner for accessing bullet data
	public TankData owner; 
}
