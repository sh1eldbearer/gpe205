﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollider : MonoBehaviour 
{
	/* Private variables */
	// 	Holds the TankData of the bullet's owner for accessing bullet data 
	private TankData tankData; 
	// Holds the numerical value of the layer that the tank that owns ("fired") this bullet resides on
	private int layer;
	// Holds the GameObject of the object the bullet collides with
	private GameObject otherObj;
	// Holds the TankData component of the object this bullet collided with
	private TankData otherTankData;

	void OnCollisionEnter (Collision other)
	{
		// Grabs and stores the TankData component of the tank that fired this bullet
		tankData = this.gameObject.GetComponent<BulletOwner> ().owner;
		// Grabs and stores the GameObject of the object the bullet collided with
		otherObj = other.gameObject;
		// Grabs and stores the layer on which the owner tank resides
		layer = otherObj.layer;
		// Grabs and stores the TankData component from the Collision object
		otherTankData = otherObj.GetComponent<TankData> ();

		// If the other object is a tank...
		if (otherTankData != null)
		{
			// ... and this bullet came from a player...
			if (LayerMask.LayerToName (layer) == "Players")
			{
				// ... and if the bullet collided with an enemy, or if the bullet collided with a player, and friendly fire has been enabled...
				if ((otherObj.layer == LayerMask.NameToLayer ("Enemies")) ||
				    (otherObj.layer == LayerMask.NameToLayer ("Players") && GameManager.gm.friendlyFire == true))
				{
					// ... tells the enemy to take damage
					otherObj.BroadcastMessage ("Damage", tankData);
				}
			}
			// ... or, if this bullet came from an enemy...
			else if (LayerMask.LayerToName (layer) == "Enemies" && otherTankData != null)
			{
				// ... and if the bullet collides with a player, or if the bullet collided with an enemy, and friendly fire has been enabled...
				if ((otherObj.layer == LayerMask.NameToLayer ("Players")) ||
				    (otherObj.layer == LayerMask.NameToLayer ("Enemies") && GameManager.gm.enemyFriendlyFire == true))
				{
					// ... tells the player to take damage
					otherObj.BroadcastMessage ("Damage", tankData);
				}
			}
		}

		/* As long as the object the bullet collided with isn't the same tank that fired the bullet (this happens if the tank shoots while 
		 * moving backwards) */
		if (otherObj != tankData.gameObject)
		{
			Destroy (this.gameObject);
		}
	}
}
