﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletLifeTimer : MonoBehaviour 
{
	/* Private variables */
	// Holds the life span of this bullet
	private float bulletLifeSpan;

	// Use this for initialization
	void Start () 
	{
		// Grabs and stores the bullet's lifespan from the owner's TankData
		bulletLifeSpan = this.gameObject.GetComponent<BulletOwner> ().owner.bulletLifespan;
	}

	void Update()
	{
		// Subtracts from the bullet's lifespan timer
		bulletLifeSpan -= Time.deltaTime;
		// When the bullet's lifespan reaches zero seconds...
		if (bulletLifeSpan <= 0f)
		{
			// ... destroys the bullet
			Destroy (this.gameObject);
		}
	}
}