﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	/* Public variables */

	// Holds this GameManager in the same memory location across all instances
	public static GameManager gm;
	// Tells the game if friendly fire is enabled
	public bool friendlyFire = false;
	// Tells the game if enemy friendly fire is enabled
	public bool enemyFriendlyFire = false;
	// Stores a list of players' TankData objects
	public List<TankData> players;
	// Stores a list of enemies' TankData objects
	public List<TankData> enemies;

	// Runs before Start()
	void Awake()
	{
		// Checks to see if any other GameManagers alreaady exist
		// If this is the first GameManager...
		if (gm == null)
		{
			// ...saves this GameManger to the static memory location...
			gm = this;
			// ...and prevents the object from being destroyed by scene changes
			DontDestroyOnLoad (gameObject);
		} 
		// If this is not the first GameManager...
		else
		{
			// ... destroys the new GameManager
			Destroy (this.gameObject);
		}
	}
}
